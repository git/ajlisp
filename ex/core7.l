;; Copyright 2016 Thomas Flynn 
;; This file is part of ajlisp.
;;
;;    ajlisp is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.
;;
;;    ajlisp is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU General Public License for more details.
;;
;;    You should have received a copy of the GNU General Public License
;;    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/


(print (and)) ;t
(print (and #t)) ;t
(print (and #f)) ;f
(print (and #t #t #t)) ;t
(print (and #t #f)) ;f
(print (and #f #t)) ;f 
(print (and #f #f)) ;f
(print (and 1 2 (list 99 3)))
(print (and 1 #f (list 111)))
