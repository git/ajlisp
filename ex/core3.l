;; Copyright 2017 Thomas Flynn 
;; This file is part of ajlisp.
;;
;;    ajlisp is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.
;;
;;    ajlisp is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU General Public License for more details.
;;
;;    You should have received a copy of the GNU General Public License
;;    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

(define a (list 1 2 3 4 5 6 7 8 ))
(print "should print #f:")
(print (null? a))
(print "should print #t:")
(print (null? (list)))

(define sum (lambda (a)
	      (if (null? a) 0
		(+ (car a) (sum (cdr a))))))

(print "should display 10:")
(display 10)
(newline)
(print "should print 36:")
(print (sum a))
(print "should display 10:")
(display 10)
(newline)
(print "should display 33:")
(display 33)
(newline)
(print "printing random number (rerun for different number)")
(print (random 100))
(print "should be #t")
(print (list? (list)))
(print "should be #t")
(print (list? (list (quote a)  (quote b))))
(print "should be #f:")
(print (list? 500))
(print "should be #f:")
(print (list? "asdsd"))
(print "should be the list (1,2,3,4,5,6,7,8)")
(print a)
(print "list stuff:")
(print "should be (1 2):")
(print (cons 1 2))
(print "should be (1 2 3):")
(print (cons 1 (cons 2 3)))
(print "should be 1:")
(print (car (cons 1 2)))
(print "should be 2:")
(print (car (cdr (cons 1 (cons 2 3)))))
(print "should be (1):")
(print (cons 1 (list)))
(print "should be #t")
(print (null? (list)))
(print "should be #t:")
(print (null? (cdr (cons 1 (list)))))

(define map (lambda (l fn)
	       (if (null? l)
		   (list)
		 (cons (fn (car l)) (map (cdr l) fn)))))

(print "should be (2 4 6 8 10):")
(print (map (list 1 2 3 4 5) (lambda (n) (* n 2))))

(print "New lambda test:")
(define lt (lambda n (+ (car n) (car (cdr n)))))
(print "should be 3:")
(print (lt 1 2))
