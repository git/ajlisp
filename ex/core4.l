;; Copyright 2016 Thomas Flynn 
;; This file is part of ajlisp.
;;
;;    ajlisp is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.
;;
;;    ajlisp is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU General Public License for more details.
;;
;;    You should have received a copy of the GNU General Public License
;;    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

10

(define repeat-n
  (lambda (n fn)
    (if (< 0 n)
	(begin
	 (fn n)
	 (repeat-n (- n 1) fn)))))

(define sp-ast
  (lambda (ns na)
    (repeat-n ns (lambda (k) (display " ")))
    (repeat-n na (lambda (k) (display "*")))
    (newline)))

(define diamond
  (lambda (nlines)
    (repeat-n nlines
	      (lambda (n)
		(sp-ast n (+ (* (- nlines n) 2) 1))))))


(diamond 10)

;(defstruct mystr ( (int x) (char c) (float y)))
;add this to the structure database

;(mem-var mystr c) ;lookup in structure database

;(mystr 2)
