/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syntax_if extends syntax_val{
	public sym_vals fn_val(LinkedList<sym_vals> vals, Env env){
		ListIterator<sym_vals> listIt = vals.listIterator();

		if(!listIt.hasNext())
	    throw new IllegalStateException("If: missing test");
		sym_vals test   = listIt.next();
		if(!listIt.hasNext())
	    throw new IllegalStateException("If: missing consequence");
		sym_vals conseq = listIt.next();
		sym_vals alt = listIt.hasNext()?listIt.next():null;
		sym_vals test_res = lisp.eval(test,env);
		if(test_res.bool_val() == true)
	    return lisp.eval(conseq,env);
		else
	    return (alt==null)?lisp.lisp_false:lisp.eval(alt,env);
	


	}
}

//
//fn: Val -> Val (+, eq, cons, car, *)
//cr: Data -> Val (if, lambda, define, let,...)
//sy: Data -> Data
