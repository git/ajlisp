/* Copyright 2017 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.Queue;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Vector;
import java.util.Iterator;
import java.io.EOFException;

interface parser {
	public LinkedList<sym_vals> parse(String program) ;
}

//given a character iterator,
//make an ast iterator.
//what this stores is basically the character iterator,
//inside a token iterator
//inside a wrapper that allows peek()ing

class std_parse implements parser, Iterator<sym_vals> {
	peekiter<loc_str> child;

	//_child should be an iterator derived from a scanner with
	//deliimiter "" (matches all).

	public std_parse(Iterator<String> _child){
		child = new peekiter(new tokenize(new strip(_child)));
	}

	public std_parse(){
		child = null;
	}
	
	public  LinkedList<sym_vals> parse(String program) {
		return
	    parse_from_tokens(
												tokenize.tokenize_by_string(
																										strip.strip_comments_by_string(program)));
	}

	
	//after tokenize we come here. turns the collection of strings into
	//a collection of sym_vals.
	public static LinkedList<sym_vals> parse_from_tokens(Queue<loc_str> tokens) {
		LinkedList<sym_vals> ret =
			new LinkedList<sym_vals>();
		//System.out.println("Starting from n tokens: " + tokens.size());
		while(tokens.size() > 0){
	    sym_vals ast = ast_from_tokens(tokens);
	    if(!(ast == null))
				ret.add(ast);
		}
		//	System.out.println("Returning " + ret.size() + " asts");
		return ret;
	}

	//each sym_vals should be considered an abstract syntax tree.
	//it is one whole lisp expression. e.g. (print (+ a (* 3 (if (a) b c))))
	//this function is recursive.
	public static sym_vals ast_from_tokens(Queue<loc_str> tokens) {
		if(tokens.size() == 0){
	    //  System.out.println("Going to throw");
	    throw new except_eof("Missing left paren ')'");
		}

		loc_str ls = tokens.remove();
	

		int line = ls.line;
		String tok = ls.str;
       
		if(tok.equals("(")){
	    

	    sym_list ret = new sym_list();
	    if(tokens.size() == 0)
				throw new except_eof("Missing ) near line " + line);
	    

	    while(!tokens.peek().str.equals( ")")){
		

				ret.children.addLast( ast_from_tokens(tokens) ); //recursive call.
		

				if(tokens.size() == 0){
		    

					throw new except_eof
						("Error: Missing ) from exp beginning at " + line);
				}
	    }
	    

	    tokens.remove();
	    

	    return ret;
		}//if( tok.equals ( "(" )...
		else{

	    return new sym_data(tok,line);
		}
	}

	public boolean hasNext(){
		return child.hasNext();
	}

	public sym_vals next()  { //should get a whole a.s.t
		if(hasNext() == false){
	    //  System.out.println("Going to throw");
	    throw new except_eof("Missing left paren ')'");
		}
		//			System.out.println("accessing the child next...");
		loc_str ls = child.next();
	
		//System.out.println("accessed...");
			if(ls == null)
				return null;
			
		int line = ls.line;
		String tok = ls.str;
       
		if(tok.equals("(")){
	    
		  sym_list ret = new sym_list();
	    if(hasNext() == false)
				throw new except_eof("Missing ) near line " + line);
	    

	    while(!child.peek().str.equals( ")")){
		

				ret.children.addLast( next() ); //recursive call.
		

				if(hasNext() == false){
		    

					throw new except_eof
						("Error: Missing ) from exp beginning at " + line);
				}
	    }
	    

	    child.next(); //flush the right paren.

	    return ret;
		}//if( tok.equals ( "(" )...
		else{ //not list.

	    return new sym_data(tok,line);
		}
	}
	
	public void remove(){
		throw new UnsupportedOperationException("Remove not implemented");
	}

}
