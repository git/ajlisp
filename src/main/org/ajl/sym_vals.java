/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.LinkedList;

public abstract class sym_vals{
	public int num_val(){
		throw new IllegalStateException("Error: not an int");
	}
	public String str_val(){
		throw new IllegalStateException("Canot convert to string");
	}
	public sym_vals fn_val(LinkedList<sym_vals> args){
		throw new IllegalStateException("Canot convert to fn: str_val is " + str_val());
	}
	public sym_vals fn_val(LinkedList<sym_vals> args, Env env){
		return fn_val(args);
	}
	
	//the three argument case is used to handle 'let'
	//the default is to call the two arg version, dropping the third arg.
	public sym_vals fn_val(LinkedList<sym_vals> args, Env env, String glyph){
		return fn_val(args,env);
	}
	
	public float float_val(){
		throw new IllegalStateException("Error: not a float");
	}
	public void disp(){
		System.out.println(str_val());
	}
	public boolean bool_val(){
		return true;
	}

  public abstract boolean eq(sym_vals rhs);

}
