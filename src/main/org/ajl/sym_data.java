/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.ListIterator;
import java.util.LinkedList;

public class sym_data extends sym_vals{
	public sym_data(String _glyph, int _line){
		glyph = _glyph;
		line = _line;
	}
	
	public sym_data(String _glyph){
		this(_glyph,-1);
	}
  
	public boolean eq(sym_vals rhs){
		return ((sym_data)rhs).glyph.equals(glyph);
	}
  
	public String str_val(){
		return glyph;
	}
    
	String glyph;
	int line;
}
