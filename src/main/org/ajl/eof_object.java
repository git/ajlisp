/* Copyright 2017 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class eof_object extends fn_vals{//no newline
    public sym_vals fn_val(LinkedList<sym_vals> args){
			if(args.size() != 1)
				throw new IllegalStateException("eof-object expects exactly 1 arg");
			if(args.getFirst() instanceof sym_eof)
				return lisp.lisp_true;

			return lisp.lisp_false;
    }
}
