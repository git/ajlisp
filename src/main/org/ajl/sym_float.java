/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

public class sym_float extends sym_vals{
    public boolean eq(sym_vals rhs){
	return ((sym_float)rhs).val == val;
    }
    public sym_float(float _val){
	val = _val;
    }
    public float float_val(){ //numeric value of a symbol
	return val;
    }
    public String str_val(){
	return Float.toString(val);
    }

    float val;
}
