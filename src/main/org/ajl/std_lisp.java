/* Copyright 2017 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Iterator;

import java.util.Scanner;

public class std_lisp{
	public Env env = lisp.standard_env();
	public std_parse thep;

	//the_input should be an iterator derived from a scanner with
	//deliimiter "" (matches all).

	public std_lisp(Iterator<String> the_input){
		thep = new std_parse(the_input);
	}

	public std_lisp(){
		thep = new std_parse();
	}
	
	//no throw, use default environment
	//requires expliti i/o
	public String runProg_nothrow(String prog){
		return lisp.runProg_nothrow(prog,env,thep);
	}
	public sym_vals runProg_throw(String prog){
		return lisp.runProg_throw(prog,env,thep);
	}
	
	public String runProg_throw(sym_vals ast){
		return lisp.eval(ast,env).str_val();
	}
}

