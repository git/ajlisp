/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syntax_define extends syntax_val{
	public sym_vals fn_val(LinkedList<sym_vals> vals,
												 Env env){

		ListIterator<sym_vals> listIt = vals.listIterator();
		if(!listIt.hasNext())
	    throw new IllegalStateException("define: missing name");
		sym_data name = (sym_data)(listIt.next());
		if(!listIt.hasNext())
	    throw new IllegalStateException("define: missing definition body");
		sym_vals val  = listIt.next();
		//	System.out.println("Defining " + name.glyph);
		env.put(name.glyph,lisp.eval(val,env)); //evaluate next data, assign to glyph.
		//the type of name.glyph is whatever eval returns.
		return env.get(name.glyph);//define at this level
	}

}
