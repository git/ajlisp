/* Copyright 2017 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.LinkedList;

public class apply extends fn_vals{
	public sym_vals fn_val(LinkedList<sym_vals> args){
		if(args.size() < 2)
	    throw new IllegalStateException
				("apply requires at least 2 arguments.");
		if(! (args.get(0) instanceof fn_vals))
	    throw new IllegalStateException
				("Invalid 1st argument to apply, should be a function");
		fn_vals f = (fn_vals)args.get(0);
		if(!(args.get(1) instanceof sym_list))
	    throw new IllegalStateException
				("Invalid 2nd argument to apply, should be a list");
		sym_list al= (sym_list)args.get(1);
		return f.fn_val(al.children);
	}
}
