/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syn_procedure extends syntax_val{//representation of macro 
	public syn_procedure(sym_vals _parm, sym_vals _body, Env _env){
		LinkedList<sym_vals> body = new LinkedList<sym_vals>();
		body.add(_body); //single body exp. for syntactic procedures.
		proc = new procedure(_parm,body,_env); //lambda ast -> ast
	}
	public sym_vals fn_val(LinkedList<sym_vals> vals,
												 Env env){ //called when macro is to be expanded
		sym_vals expanded = proc.fn_val(vals,env); //expand macro
		//expanded is the new ast.
	
		Env e = env.is_defined("*macro-debug*");
		if(e != null){
	    if(e.get("*macro-debug*") == lisp.lisp_true){
				System.out.println("BEGIN DBG:");
				System.out.println(expanded.str_val());
				System.out.println("END DBG");
	    }
		}
		//now we evaluate the new ast, and return this.
		return lisp.eval(expanded,env);//eval expansion

	}
	procedure proc;
}
