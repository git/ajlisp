/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.ListIterator;
import java.util.LinkedList;

public class eq extends fn_vals{
    public sym_vals fn_val(LinkedList<sym_vals> args){
	sym_vals lhs = args.get(0);
	sym_vals rhs = args.get(1);
	if(lhs.getClass() != rhs.getClass())
	    return lisp.lisp_false;
	
	return new sym_bool( lhs.eq(rhs) );					
	/*	return new sym_bool( //number equalit
			   args.get(0).num_val() ==
			    args.get(1).num_val()
			    );*/
	
    }
    
    
}
