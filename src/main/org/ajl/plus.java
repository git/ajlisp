/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.LinkedList;

public class plus extends fn_vals{
    public sym_vals fn_val(LinkedList<sym_vals> args){
	sym_vals lhs = args.get(0);
	sym_vals rhs = args.get(1);
	if((lhs instanceof sym_num) &&
	   (rhs instanceof sym_num))
	   return new sym_num(lhs.num_val() +
			      rhs.num_val());
	else
	    return new sym_float(lhs.float_val() + rhs.float_val());
	
	
    }
}
