/* Copyright 2017 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;
import java.util.Iterator;
import java.util.Queue;
import java.util.LinkedList;

//tokenize. take an iterator of strings (each 1 char long) and
//construct iterator of loc_str.
//for next step in parsing, see parser.java

class tokenize implements Iterator<loc_str>{
	Iterator<String> child; //this will be a string stripped of comments

	// there are two modes the tokenizer goes in: 'top' and 'quote'.
	// mode 'quote' is entered when scanning characters between a
	// pair of double quotes.

	int QUOTE=1;
	int TOP=0;
	int MODE=TOP; 
	int line=0;
		
	int depth=0;
		
	String cstr="";
	
	Queue<loc_str> waiting =
		new LinkedList<loc_str>();
	
	public tokenize(Iterator<String> _child){ //_child: a program stripped of comments
		child = _child;
	}

	public boolean hasNext(){
		return ( (child.hasNext()) ||
						 (waiting.size() > 0));
	}

	public loc_str next(){
	 
		if(waiting.size() == 0){
			
			if(child.hasNext() == false) //its weird that this can return null.
				return null;
			
			boolean scanning=true;
		 
			while( (scanning==true)
						 &&
						 (child.hasNext() == true)){
				//				System.out.println("Scaning & hasnext...");
				char nxt = child.next().charAt(0); //the next character.
				
				if(util.is_new_line(nxt))
					line++;
				
				// System.out.println("Char: " + str.charAt(cpos));
				// System.out.println("Mode: " + MODE);
				if(MODE==TOP){
					if(util.is_white_space(nxt)){
						scanning=false;
						add_to_queue(cstr,line,waiting);
						cstr="";
						//stay in top mode.
					}  
					else if(util.is_dquote(nxt)){ //start a string
						scanning=false;
						add_to_queue(cstr,line,waiting);					
						cstr ="\"";
						depth++;
						MODE=QUOTE; //enter quote mode.
					}
					else if(nxt == '('){
						scanning=false;
						//System.out.println("Doing an lparent. Current str is " + cstr);
						add_to_queue(cstr,line,waiting); //<--add to RET 
						add_to_queue("(",line,waiting); //<--add to RET
						cstr="";
						depth++;
					}
					else if(nxt == ')'){
						scanning=false;
						//  System.out.println("Doing an rparn. Current str is " + cstr);
						add_to_queue(cstr,line,waiting); //<--add to RET
						add_to_queue(")",line,waiting); //<--add to RET
						cstr="";
						depth--;
					}
					else{
						//   System.out.println("Doing normal current str is " + cstr);
						cstr = cstr + nxt; //add the current character
						//   System.out.println("After noral: Current str is " + cstr);
					}
				}
				else if (MODE==QUOTE){
					if(util.is_dquote(nxt)){//got the closing quote
						scanning=false;
						cstr=cstr + "\"";
						add_to_queue(cstr,line,waiting); //<--add to RET.
						cstr="";
						depth--;
						MODE=TOP;
					}
					else{ 
						cstr = cstr + nxt;
					}
				}
			}
		}
		loc_str ret;
		if(waiting.size() > 0) //if we just pushed something, return it.
			ret=waiting.remove();
		else
			ret=next(); //else?
		//		System.out.println("Will remove");
		return ret;
	}		
	
	public void remove(){
		throw new UnsupportedOperationException("Remove not implemented");
	}
					 
	//this can give some empty things...
	public static void add_to_queue(String cstr, int line, Queue<loc_str> q){
			if(cstr.length () > 0){
	//	    System.out.println("Adding " + cstr);
				q.add(new loc_str(cstr,line));
			}
	}

	//non-stream based tokenizer. legacy systemz ya heard
	//non-recursive, makes a single pass through str
	public static Queue<loc_str> tokenize_by_string(String str){
		//ex str = (+ (* 2 3) 1)
		//System.out.println("tokenize a string of length n:" + str.length());
		int cpos=0;
		Queue<loc_str> ret = new LinkedList<loc_str> ();
		String cstr="";

		int QUOTE=1;
		int TOP=0;
		// there are two modes the tokenizer goes in: 'top' and 'quote'.
		// mode 'quote' is entered when scanning characters between a
		// pair of double quotes.
		int MODE=TOP; 
		int line=0;
		
		int depth=0;
		
		while(cpos < str.length()){
	    if(util.is_new_line(str.charAt(cpos)))
				line++;
	    // System.out.println("Char: " + str.charAt(cpos));
	    // System.out.println("Mode: " + MODE);
	    if(MODE==TOP){
				if(util.is_white_space(str.charAt(cpos))){
					//<--add to RET
					add_to_queue(cstr,  //current string (token)
											 line,  //line number
											 ret);  //the queue to add to
					cstr="";
					cpos++;
					//stay in top mode.
				}  
				else if(util.is_dquote(str.charAt(cpos))){ //start a string
					add_to_queue(cstr,line,ret); //<--add to RET
					cstr ="\"";
					cpos++;
					depth++;
					MODE=QUOTE; //enter quote mode.
				}
				else if(str.charAt(cpos) == '('){
					//System.out.println("Doing an lparent. Current str is " + cstr);
					add_to_queue(cstr,line,ret); //<--add to RET 
					add_to_queue("(",line,ret); //<--add to RET
					cstr="";
					cpos++;
					depth++;
				}
				else if(str.charAt(cpos) == ')'){
					//  System.out.println("Doing an rparn. Current str is " + cstr);
					add_to_queue(cstr,line, ret); //<--add to RET
					add_to_queue(")",line,ret); //<--add to RET
	
					cstr="";
					depth--;
					cpos++;
				}
				else{
					//   System.out.println("Doing normal current str is " + cstr);
					cstr = cstr + str.charAt(cpos); //add the current character
					//   System.out.println("After noral: Current str is " + cstr);
					cpos++;
				}
	    }
	    else if (MODE==QUOTE){
				if(util.is_dquote(str.charAt(cpos))){//got the closing quote
					cstr=cstr + "\"";
					add_to_queue(cstr,line,ret); //<--add to RET.
					cstr="";
					depth--;
					MODE=TOP;
					cpos++;
				}
				else{ 
					cstr = cstr + str.charAt(cpos);
					cpos++;
				}
	    }
		}
		if( (cstr.length() > 0) && (depth == 0) ) //add whatever is remaining.
			add_to_queue(cstr,line,ret); //<--RET

		return ret;
	}

}
