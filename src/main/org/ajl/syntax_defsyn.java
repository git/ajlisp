/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

//define-syntax handler
public class syntax_defsyn extends syntax_val{ 
	public sym_vals fn_val(LinkedList<sym_vals> vals,
												 Env env){

		ListIterator<sym_vals> listIt = vals.listIterator();
		if(!listIt.hasNext())
	    throw new IllegalStateException
				("define-syntax: missing name");
		sym_vals name = listIt.next(); //name of the syntax def
		if(!(name instanceof sym_data))
	    throw new IllegalStateException
				("define-syntax: invalid form of name");

		if(!listIt.hasNext())
	    throw new IllegalStateException
				("define-syntax: missing parameter name");
		sym_vals parm = listIt.next(); //parameter list of syntax
	
		if(!listIt.hasNext())
	    throw new IllegalStateException
				("define-syntax: missing definition body");
	
		sym_vals val = listIt.next();//body of the syntax definition

		env.put(name.str_val(),
						new syn_procedure(parm,val,env));
	
		return env.get(name.str_val());//define at this level
	}
}
	
