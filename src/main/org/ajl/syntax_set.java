/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syntax_set extends syntax_val{
	public sym_vals fn_val(LinkedList<sym_vals> vals,
												 Env env){

		ListIterator<sym_vals> listIt = vals.listIterator();

		//	    System.out.println("Doing set!");
		if(!listIt.hasNext())
	    throw new IllegalStateException("set!: missing name");
		sym_data name = (sym_data)(listIt.next());
		String name_glyph = name.glyph;
		if(!listIt.hasNext())
	    throw new IllegalStateException("set!: missing expression");
	
		sym_vals val = listIt.next();
		env.find(name_glyph).put(name_glyph,lisp.eval(val,env));
		//	    System.out.println("Completed set!");
		return env.find(name_glyph).get(name_glyph);


	}
}
