/* Copyright 2017 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.Scanner;
import java.util.LinkedList;

public class read extends fn_vals{
	public static std_parse thep;
	
	public read(){
		thep =
			new std_parse( (new Scanner(System.in)).useDelimiter(""));
	}
	
	public sym_vals fn_val(LinkedList<sym_vals> args){
		sym_vals ret=null;
		if(thep.hasNext())
			ret = thep.next();
		
		if(ret == null) //its weird that we have to check for this, but OK...
			return new sym_eof();
		
		return ret;
	}
}
