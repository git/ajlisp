/* Copyright 2017 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.Iterator;
import java.util.Queue;
import java.util.LinkedList;


public class peekiter<T> implements Iterator<T>
{
	private Iterator<T> scan;
	private T next;
	boolean peeked;
	
	public peekiter( Iterator<T> _scan  )
    {
			scan = _scan;
			peeked=false;
	  }
	
	
	public boolean hasNext() //from java docs: "returns true if next() would return an element rather than throwing an exception."
    {
			if(peeked){
        return true;
			}
			else{
				return scan.hasNext();
			}
			
    }

    public T next()
    {
			if(peeked){
				peeked=false;
				return next;
			}
			else{
				return scan.next();
			}
	  }

    public T peek()
    {
			if(!peeked){
				next = scan.next();
				peeked=true;
			}
			return next;
    }

	public void remove(){
		throw new UnsupportedOperationException("Remove not implemented");
	}

}
