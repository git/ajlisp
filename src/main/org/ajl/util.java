/* Copyright 2017 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;
	
class util {
	public static boolean is_new_line(char c){
		if(c == '\r')
	    return true;
		if(c == '\n')
	    return true;
		return false;
	}
	
	
	
	public static boolean is_dquote(char c){
		if(c == '"')
	    return true;
		return false;
	}
	
	public static boolean is_white_space(char c){
		if (is_new_line(c))
	    return true;
		if( c == ' ')
	    return true;
		if(c == '\t')
	    return true;
		return false;
	}
	
	//returns n where str[n] == '\n' or str[n] == '\r' or n = str.length()
	public static int until_new_line(int strt, String str){
		int ret=strt;
		while( ret < str.length() &&
					 !is_new_line(str.charAt(ret) ) ) ret++;
		return ret;
	}

}
