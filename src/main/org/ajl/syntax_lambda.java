/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.LinkedList;
import java.util.ListIterator;

public class syntax_lambda extends syntax_val{
    public sym_vals fn_val(LinkedList<sym_vals> vals,
			   Env env){

	ListIterator<sym_vals> listIt = vals.listIterator();
	if(!listIt.hasNext())
	    throw new IllegalStateException
		("Missing parameter definition for lambda");
    
	sym_vals parms = listIt.next();
    
	if(!(parms instanceof sym_list) &&
	   !(parms instanceof sym_data))
	    throw new IllegalStateException
		("Invalid form of lambda parameter list");
    
	if(!listIt.hasNext())
	    throw new IllegalStateException
		("Missing body of lambda expression");
    
    
	LinkedList<sym_vals> body = new LinkedList<sym_vals>(); //store list of exprs
	while(listIt.hasNext())
	    body.add(listIt.next());
    
	return new procedure(
			     parms,
			     body,
			     env);
    
    }

}
