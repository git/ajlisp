/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.LinkedList;

public class list_null extends fn_vals{    //get rest
    public sym_vals fn_val(LinkedList<sym_vals> args){
	sym_vals first = args.get(0);
	if(first instanceof sym_list)
	    if( ((sym_list)first).children.size() == 0)
		return lisp.lisp_true;
	return lisp.lisp_false;
    }
}
