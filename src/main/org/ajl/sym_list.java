/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.ListIterator;
import java.util.LinkedList;

public class sym_list extends sym_vals{
	public sym_list(LinkedList<sym_vals> _children, int _line){
		line = _line;
		children = _children;
	}
	public sym_list(LinkedList<sym_vals> _children){
		this(_children,-1);
	}
	public sym_list(){
		this(new LinkedList<sym_vals>());
	}
	public boolean eq(sym_vals rhs){
		return false;
	}
	public String str_val(){
		String ret;
		ret = "( ";
		ListIterator<sym_vals> listIt = children.listIterator();
		while(listIt.hasNext())
	    ret += listIt.next().str_val() + " ";
		ret += ")";
		return ret;
	
	}

	int line;
	LinkedList<sym_vals> children = new LinkedList<sym_vals>();
}
