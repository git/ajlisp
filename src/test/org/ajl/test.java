/* Copyright 2017 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

package org.ajl;

import java.util.Queue;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;

import org.ajl.Env;

public class test
{


	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in).useDelimiter("");

		std_lisp mylisp = new std_lisp(sc);

		if(args.length < 1){
			interpreter(mylisp);
			return;
		}
		else{

			try{
				mylisp.runProg_throw("(load \"" + args[0] + "\")");
			}
			catch(Throwable e){
				System.out.println("CAUGHT ERROR:");
				System.out.println(e);
				if(e instanceof StackOverflowError)
					System.out.println("Possible infinite loop?");
				e.printStackTrace(System.out);
			}
		}
	}
     
	public static void printArr(String [] mystr){
		int i;
		for(i=0; i < mystr.length; i++)
			System.out.println("I: " + i + " Len:  " + mystr[i].length() + " " + mystr[i]);
	}
	
	public static void interpreter(std_lisp alisp){
		//scanner implements iterator<string>  + other stuff
		//related to parsing strings.
		//e.g. converting fro strings to different types
		

		/*		tokenize tkn = new tokenize(new strip(sc.useDelimiter("")));
		while(tkn.hasNext()){
			System.out.println("Next loc:");
			loc_str lstr = tkn.next();
			if(lstr != null)
				System.out.println(tkn.next().str);
				}*/

		while(true){			
			System.out.print(":~) ");
			try{
				System.out.println(	alisp.runProg_throw(alisp.thep.next()));
			}
			catch(except_eof e){
				System.out.println("Program text ended prematurely.");
				e.printStackTrace(System.out);
				return;
			}
			catch(Throwable e){
				System.out.println("CAUGHT ERROR:");
				if(e instanceof StackOverflowError)
					System.out.println("Possible infinite loop?");
				e.printStackTrace(System.out);
			}
		 

		}
	}
}
