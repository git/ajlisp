/* Copyright 2016 Thomas Flynn */
/*This file is part of ajlisp.

    ajlisp is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ajlisp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ajlisp.  If not, see <http://www.gnu.org/licenses/>.*/

import org.ajl.fn_vals;
import org.ajl.std_lisp;
import org.ajl.sym_vals;
import org.ajl.sym_str;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

class date extends fn_vals{
	public sym_vals fn_val(LinkedList<sym_vals> args){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date now = new Date();
		String strDate = sdf.format(now);
		return new sym_str(strDate);
	}
	
}

public class scriptable {

	public static void main(String argv[]) {
		std_lisp mylisp = new std_lisp();
		mylisp.env.put("date",new date());

		String prog =
			"(print \"Hello world. Here is the current date and time: \") " +
			"(print (date))" + 
			"(print \"Have a nice day!\")";
		mylisp.runProg_nothrow(prog);
	}
}
