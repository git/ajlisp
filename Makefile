default:
	javac -Xlint:unchecked -d ./bin/main src/main/org/ajl/*.java
	jar -cvf ajl.jar -C bin/main org/
	javac -Xlint:unchecked -classpath ./ajl.jar -d ./bin/test src/test/org/ajl/*.java
clean:
	rm bin/main/org/ajl/*.class
	rm bin/test/org/ajl/*.class
